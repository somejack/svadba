[<< späť](../)

# Dary

| Vladkovi ľudia                      |    € |
| ----------------------------------- | ---: |
| Dedko Janko a Babka Anka            |  500 |
| Krstný Jozef a Krstná Tonka         |  300 |
| Tomáš Šomják a Janka Šomjáková      |  150 |
| Krstná Mária a Krstný Juraj         |  500 |
| Monika Hovancová                    |  100 |
| Marienka Hrebíková a Michal Hrebík  |  100 |
| Janka Rabiková a Maroš Rabik        |  100 |
| Marienka Olejníková a Miro Olejník  |  250 |
| Mirka Olejníková a Rado Cibík       |  150 |
| Mário Olejník a Daniela Kovalčíková |   50 |
| Michal Čuba a Marienka Čubová       |  200 |
| Paťo Čuba                           |   50 |
| Dominik Čuba a Janka                |  100 |
| Marienka Kažová a Miro Kaža         |  200 |
| Zuzka Vitkajová a Vlado Vitkaj      |  300 |
| Dušan Ščecina a Ilonka Ščecinová    |  500 |
| Jano Pavlík a Miluša Pavlíková      |  200 |
| Helena Gmitrová a Milan Gmitro      |  500 |
| Lucka Ženčuchová a Miro Ženčuch     |  100 |
| Katka Vašková a Lukáš Vaško         |  100 |
| Milan Gmitro a Ivka                 |  100 |
| Eva Letanovská a Jozef Letanovský   |  200 |
| Iľka                                |   20 |
| **Spolu**                           | 4770 |

---

| Lenkini ľudia                                   |   € |
| ----------------------------------------------- | --: |
| Braček Tomík a Dadka                            | 100 |
| Milenka Najduchová a Jano Najduch               | 100 |
| Mirka Pavlíková a Miloš Pavlík                  | 100 |
| Saška Shamsaldeen Kurtyová a Mammad Shamsaldeen | 100 |
| Anička Veverková a Tomáš Veverka                | 100 |
| Adélka                                          |  50 |
| Michal Kurty a Táňa Kurtyová                    |  30 |
| Jožko Skirka a Jana Skirková                    | 100 |
| Oľga Kováčová a Štefan Kováč                    | 100 |
| Lenka Pohlová a Peťo Ferko                      |  50 |
| Michaela Fugová a Peter Fuga                    | 100 |
| **Spolu**                                       | 930 |

---

**Redový spolu**: 1200€

---

**Celkovo**: 6900€
