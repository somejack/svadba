[<< späť](../)

# Kostol

Farský chrám apoštolov Petra a Pavla  
Toryská 1/D  
040 11 Košice  

![kostol](./images/kostol.png)

![mapka](./images/kostol_mapka.png)