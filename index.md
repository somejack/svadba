# Vitajte!

Tu nájdete potrebné informácie k našej svadbe,  
ktorá sa uskutoční dňa 5.5.2018 o 16:00.

[Kostol](./kostol)

[Sála](./sala)

[Zasadací poriadok](./zasadaci_poriadok)

[Náš domček](./domcek)

[Pesničky](./pesnicky)

LEBO V+L=VL 😍
